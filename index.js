// function diagonalDifference(arr) {
//   const length = arr.length;
//   let first = 0;
//   let last = 0;
//   for (let i = 0; i < length; i++) {
//     first += arr[i][i];
//     last += arr[length - i - 1][i];
//   }
//   return Math.abs(first - last);
// }
// matrix = [
//   [11, 2, 4],
//   [4, 5, 6],
//   [10, 8, -12],
// ];
// diagonalDifference(matrix);

function plusMinus(arr) {
  let positiveNum = 0,
    negativeNum = 0,
    zeroNum = 0;
  const length = arr.length;
  for (let i = 0; i < length; i++) {
    if (arr[i] > 0) {
      positiveNum += 1;
    } else if (arr[i] < 0) {
      negativeNum += 1;
    } else {
      zeroNum += 1;
    }
  }
  var positiveRatios = (positiveNum / length).toFixed(6);
  var negativeRatios = (negativeNum / length).toFixed(6);
  var zeroRatios = (zeroNum / length).toFixed(6);
  console.log(positiveRatios, negativeRatios, zeroRatios);
  return positiveRatios, negativeRatios, zeroRatios;
}

plusMinus([1, 2, 3, -1, -2, -3, 0, 0]);
